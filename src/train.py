from init import *
from model import *
from data_utils.get_data import *
from data_utils.dataloader import *
from tools.trainer import *
from tools.config import *

import yaml

import argparse

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--config', required=True)
    # parser.add_argument('--checkpoint', required=True)

    args = parser.parse_args()
    config = Cfg.load_config_from_file(args.config)

    translation_dict = get_datatransforms(config['path_labels'])
    train_data = Datasets(path=config['path_img'], config=config, translation_dict = translation_dict)
    train_loader = DataLoader(train_data, 
                            batch_size=config["batch_size"], 
                            shuffle=True, 
                            num_workers=1, 
                            drop_last=True)


    model = MultilabelClassifier(2,2,2,2).to(config['device'])
    Trainer_ = Trainer(config, model)
    checkpoint_losses = Trainer_.train(train_loader)

    with open(config['path_save_weight'],'wb') as open_file:
        torch.save(model, open_file)
if __name__=="__main__":
    main()
    




