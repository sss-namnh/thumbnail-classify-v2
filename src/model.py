from init import *



class MultilabelClassifier(nn.Module):
    def __init__(self, n_model, n_product, n_other, n_sceenshot):
        super().__init__()
        self.resnet = models.resnet34(pretrained=True)
        self.model_wo_fc = nn.Sequential(*(list(self.resnet.children())[:-1]))

        self.model = nn.Sequential(
            nn.Dropout(p=0.2),
            nn.Linear(in_features=512, out_features=n_model)
        )

        self.product_only = nn.Sequential(
            nn.Dropout(p=0.2),
            nn.Linear(in_features=512, out_features=n_product)
        )
        self.other = nn.Sequential(
            nn.Dropout(p=0.2),
            nn.Linear(in_features=512, out_features=n_other)
        )

        self.screenshot = nn.Sequential(
            nn.Dropout(p=0.2),
            nn.Linear(in_features=512, out_features=n_sceenshot)
        )     

    def forward(self, x):
        x = self.model_wo_fc(x)
        x = torch.flatten(x, 1)

        return {
            'model': self.model(x),
            'product_only': self.product_only(x),
            'other': self.other(x),
            'screenshot': self.screenshot(x)
        }

