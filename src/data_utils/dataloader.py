from src.init import *
from data_utils. get_data import *

class Datasets(Dataset):

    def __init__(self , path, config, translation_dict = None):
        self.path = path
        self.config = config
        
        if os.path.isfile(self.path):

            self.folder = [self.path.split('/')[0]]

        else:
            self.folder = [x for x in os.listdir(self.path)]


        self.transform = transforms.Compose([
                transforms.Resize((224,224)),
                transforms.ToTensor(),
                transforms.Normalize((0.5,0.5,0.5), (0.5,0.5,0.5))
                                                                        ])
        self.translation_dict = translation_dict
    

    def __len__(self):
        return len(self.folder)

    def __getitem__(self, idx):
 
        if os.path.isfile(self.path):
            self.img_loc = self.path

        else:
            self.img_loc = os.path.join(self.path, self.folder[idx])
            
        image = Image.open(self.img_loc).convert("RGB")
        single_img = self.transform(image)


        if self.translation_dict == None:
            return {'images': single_img}


        N_class = self.config['num_labels']

        labels = []
        for i in range(N_class):
            labels.append(self.translation_dict[self.folder[idx]][i])

        sample = {'images': single_img,
                'labels': {
                'label_product_only': labels[0],
                'label_model': labels[1],
                'label_screenshot': labels[2],
                'label_other': labels[3]}
        }

        return sample
