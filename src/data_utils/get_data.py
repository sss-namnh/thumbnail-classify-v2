from init import *
import pandas as pd
import sys
sys.path.append("..")


def get_datatransforms(filename):
    df = pd.read_csv(filename)
    filename, product, model, screenshot, other = [], [], [], [], []
    
    for i in range(len(df)):
        filename.append(df['filename'][i])
        product.append(df['product_only'][i])
        model.append(df['model'][i])
        screenshot.append(df['screenshot'][i])
        other.append(df['other'][i])

    translation_dict = dict(zip(filename, list(zip(product, model, screenshot, other))))

    return translation_dict


