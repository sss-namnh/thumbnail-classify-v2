from init import *
from model import *
from data_utils.get_data import *
from data_utils.dataloader import *

class Trainer():
  def __init__(self, config, model, pretrained=False):
    self.config = config
    self.model = model
    self.device = config['device']

  def criterion(self, loss_func, outputs,labels):
    losses = 0
    for i, key in enumerate(outputs):
      losses += loss_func(outputs[key], labels[f'label_{key}'].to(self.device))
    return losses

  def train(self, train_loader):
    num_epochs = self.config['epochs']
    losses = []
    checkpoint_losses = []

    optimizer = torch.optim.Adam(self.model.parameters(), lr=self.config['lr_rate'])
    n_total_steps = len(train_loader)

    loss_func = nn.CrossEntropyLoss()

    for epoch in range(num_epochs):
      for i, pictures in enumerate(train_loader):
          images = pictures['images'].to(self.device)
          labels = pictures['labels']

          outputs = self.model(images)
          loss = self.criterion(loss_func,outputs, labels)
          losses.append(loss.item())

          optimizer.zero_grad()
          loss.backward()
          optimizer.step()

          if (i+1) % (int(n_total_steps/1)) == 0:
              checkpoint_loss = torch.tensor(losses).mean().item()
              checkpoint_losses.append(checkpoint_loss)
              print (f'Epoch [{epoch+1}/{num_epochs}], Step [{i+1}/{n_total_steps}], Loss: {checkpoint_loss:.4f}')
    return checkpoint_losses

