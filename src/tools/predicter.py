from init import *
from model import *
from data_utils.get_data import *
from data_utils.dataloader import *

class Predicter():
    def __init__(self, config, model):
        self.device = config['device']
        self.config = config
        self.model = model

    def predict_only_images(self, path_img):
        sample = Datasets(path = self.config['path_test'], config=self.config)
        test_loader = DataLoader(sample)
        for pictures in test_loader:

            images = pictures['images'].to(self.device)
            outputs = self.model(images)
            for i, out in enumerate(outputs):
                _, predicted = torch.max(outputs[out],1)
                print(out, predicted[0])
            break


    