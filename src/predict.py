from init import *
from model import *
from data_utils.get_data import *
from data_utils.dataloader import *
from tools.predicter import *
from tools.config import *

import yaml

import argparse

def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--config', required=True)
    # parser.add_argument('--checkpoint', required=True)

    args = parser.parse_args()
    config = Cfg.load_config_from_file(args.config)


    model = torch.load(config['path_save_weight'])

    Predicter_ = Predicter(config, model)
    Predicter_.predict_only_images(config['path_test'])

if __name__=="__main__":
    main()
    




